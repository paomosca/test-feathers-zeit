const persons = require("./persons/persons.service.js");
// eslint-disable-next-line no-unused-vars
module.exports = function(app) {
  app.configure(persons);
};
